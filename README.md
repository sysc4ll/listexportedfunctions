# ListExportedFunctions

Outil pour Windows avec GUI codé en C et l'api win32 pour lister les fonctions exportées d'une DLL et obtenir l'adresse et le nom des fonctions.

![screenshot](./screenshot.png)
